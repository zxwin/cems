<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>com.cems</groupId>
    <artifactId>cems</artifactId>
    <version>1.0</version>
    <packaging>pom</packaging>
    <name>cems</name>
    <url>http://maven.apache.org</url>
    <description>
        整个cems项目的父项目，用于管理依赖信息、版本、插件等。
    </description>

    <modules>
        <module>cems-plugin-sdk</module>
        <module>cems-plugins</module>
        <module>cems-daqp</module>
    </modules>

    <parent>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-parent</artifactId>
        <version>3.2.0</version>
    </parent>

    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <maven.compiler.source>17</maven.compiler.source>
        <maven.compiler.target>17</maven.compiler.target>

        <!-- spring相关 -->
        <spring-cloud-alibaba.version>2022.0.0.0</spring-cloud-alibaba.version>
        <spring-cloud.version>2023.0.0</spring-cloud.version>
        <springfox-boot.version>3.0.0</springfox-boot.version>

        <!-- 工具相关 -->
        <guava.version>31.0.1-jre</guava.version>
        <commons-pool2.version>2.11.1</commons-pool2.version>
        <javax-validation.version>2.0.1.Final</javax-validation.version>
        <springdoc.version>2.3.0</springdoc.version>
        <springfox-swagger2.version>3.0.0</springfox-swagger2.version>

        <!-- 数据库/存储相关 -->
        <mysql-connect.version>8.0.33</mysql-connect.version>
        <mybaits-plus-spring-boot.version>3.5.3.2</mybaits-plus-spring-boot.version>
        <influxdb-client.version>6.12.0</influxdb-client.version>
        <pagehelper-spring-boot.version>1.4.3</pagehelper-spring-boot.version>
        <mybatis-spring.version>3.0.3</mybatis-spring.version>
        <durid-spring-boot.version>1.2.20</durid-spring-boot.version>
        <spring-boot-redis.version>3.2.0</spring-boot-redis.version>
        <springfox-swagger-ui.version>3.0.0</springfox-swagger-ui.version>

        <!-- 通信/协议相关 -->
        <paho-mqtt.version>1.2.5</paho-mqtt.version>
        <openfeign.version>3.1.8</openfeign.version>
    </properties>

    <dependencyManagement>
        <dependencies>
            <!-- spring相关 -->
            <dependency>
                <groupId>com.alibaba.cloud</groupId>
                <artifactId>spring-cloud-alibaba-dependencies</artifactId>
                <version>${spring-cloud-alibaba.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>

            <dependency>
                <groupId>org.springframework.cloud</groupId>
                <artifactId>spring-cloud-dependencies</artifactId>
                <version>${spring-cloud.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>

            <!-- 工具相关 -->
            <dependency>
                <groupId>com.google.guava</groupId>
                <artifactId>guava</artifactId>
                <version>${guava.version}</version>
            </dependency>

            <dependency>
                <groupId>org.springdoc</groupId>
                <artifactId>springdoc-openapi-starter-webmvc-ui</artifactId>
                <version>${springdoc.version}</version>
            </dependency>

            <dependency>
                <groupId>org.apache.commons</groupId>
                <artifactId>commons-pool2</artifactId>
                <version>${commons-pool2.version}</version>
            </dependency>

            <dependency>
                <groupId>com.alibaba</groupId>
                <artifactId>druid-spring-boot-3-starter</artifactId>
                <version>${durid-spring-boot.version}</version>
            </dependency>
            <!-- 数据库/存储相关 -->
            <dependency>
                <groupId>com.influxdb</groupId>
                <artifactId>influxdb-client-java</artifactId>
                <version>${influxdb-client.version}</version>
            </dependency>

            <dependency>
                <groupId>mysql</groupId>
                <artifactId>mysql-connector-java</artifactId>
                <version>${mysql-connect.version}</version>
            </dependency>

            <dependency>
                <groupId>com.baomidou</groupId>
                <artifactId>mybatis-plus-boot-starter</artifactId>
                <version>${mybaits-plus-spring-boot.version}</version>
            </dependency>
        </dependencies>
    </dependencyManagement>

    <dependencies>
        <dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
        </dependency>
    </dependencies>

    <build>
        <resources>
            <resource>
                <directory>src/main/resources</directory>
                <filtering>true</filtering>
            </resource>
        </resources>
    </build>
</project>
